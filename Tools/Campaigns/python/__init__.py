# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from .MC16 import MC16a, MC16d, MC16e
from .MC20 import MC20a, MC20d, MC20e

__all__ = [
  'MC16a', 'MC16d', 'MC16e',
  'MC20a', 'MC20d', 'MC20e'
]
