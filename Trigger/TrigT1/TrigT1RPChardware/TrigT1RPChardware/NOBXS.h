/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TrigT1RPChardware_NOBXS_H
#define TrigT1RPChardware_NOBXS_H
const int NOBXS=8;
const int BCZERO=3;
#endif
