#!/bin/sh
#
# art-description: Run mc20e reco MC setup with pileup and trigsplit workflow.
# art-output: log.*
# art-athena-mt: 4
# art-type: grid
# art-include: master/Athena

export TRF_ECHO=True;
Reco_tf.py --AMI=r12627 --outputRDOFile=myRDO.pool.root --outputAODFile=myAOD.pool.root --outputESDFile=myESD.pool.root --inputHITSFile=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/RecJobTransformTests/mc16_13TeV/valid1/valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e4993_s3227/HITS.12560240._000287.pool.root.1 --inputHighPtMinbiasHitsFile=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/RecJobTransformTests/mc16_13TeV/mc16_13TeV.429884.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_highjetphoton.simul.HITS.e8314_e7400_s3508/HITS.24109349._010870.pool.root.1 --inputLowPtMinbiasHitsFile=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/RecJobTransformTests/mc16_13TeV/mc16_13TeV.429885.Epos_minbias_inelastic_lowjetphoton.simul.HITS.e8314_e7400_s3508/HITS.24109354._011911.pool.root.1 --maxEvents=500 --jobNumber=1 --imf False --steering RAWtoESD:in-RDO RAWtoESD:in-BS RAWtoESD:in+RDO_TRIG
RES=$?
echo "art-result: $RES Reco"
if [ $RES -ne 0 ];then
return $RES
fi
