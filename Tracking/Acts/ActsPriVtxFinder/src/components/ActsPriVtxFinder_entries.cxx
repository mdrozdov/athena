/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

//#include "ActsPriVtxFinder/ActsPrimaryVtxFinderAlg.h"
#include "../ActsAdaptiveMultiPriVtxFinderTool.h"
#include "../ActsIterativePriVtxFinderTool.h"

// DECLARE_COMPONENT( ActsPrimaryVtxFinderAlg )
DECLARE_COMPONENT( ActsAdaptiveMultiPriVtxFinderTool )
DECLARE_COMPONENT( ActsIterativePriVtxFinderTool )
